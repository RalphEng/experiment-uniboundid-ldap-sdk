Experiment "UniboundID LDAP SDK for Java"
=========================================

The [UniboundID LDAP SDK for Java](https://ldap.com/unboundid-ldap-sdk-for-java/) provides an InMemory LDAP Server.

```
<dependency>
    <groupId>com.unboundid</groupId>
	<artifactId>unboundid-ldapsdk</artifactId>
	<version>5.1.2</version>
</dependency>
```
_It is just a single dependency, but it is huge: 4MB._ 

The UniboundID InMemmoryLdap is one (maybe the preferred) Embedded Ldap Repository of [Spring LDAP](https://spring.io/projects/spring-ldap) (2.x.x).
see https://docs.spring.io/spring-ldap/docs/2.3.3.RELEASE/reference/#spring-ldap-testing-unboundid
Spring setup is done in `org.springframework.boot.autoconfigure.ldap.embedded.EmbeddedLdapAutoConfiguration` (`spring-boot-autoconfigure-2.4.0.jar`).


`InMemoryDirectoryServer`
-------------------------

`com.unboundid.ldap.listener.InMemoryDirectoryServer`

> This class provides a utility that may be used to create a simple LDAP server instance that will hold all of its information in memory.
> It is intended to be very easy to use, particularly as an embeddable server for testing directory-enabled applications.
> It can be easily created, configured, populated, and shut down with only a few lines of code, and it provides a number of convenience methods that can be very helpful in writing test cases that validate the content of the server. 
([Javadoc](https://docs.ldap.com/ldap-sdk/docs/javadoc/com/unboundid/ldap/listener/InMemoryDirectoryServer.html))


Commandline Tool
----------------

`com.unboundid.ldap.listener.InMemoryDirectoryServerTool`


Resources
---------

- https://github.com/pingidentity/ldapsdk

- https://stackoverflow.com/questions/27530999/spring-ldap-unit-testing-with-custom-schema-definition

Resources for Spring (Boot) and LDAP
------------------------------------

https://memorynotfound.com/spring-ldap-spring-boot-embedded-ldap-configuration-example/

https://www.i-love-software-engineering.de/testing-spring-controllers-with-spring-security-embedded-ldap-and-different-application-contexts/