package de.humanfork.experiment.ldapsdk;

import org.junit.jupiter.api.Test;

import com.unboundid.ldap.listener.InMemoryDirectoryServer;
import com.unboundid.ldap.sdk.LDAPConnection;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.util.LDAPTestUtils;

public class InMemoryDirectoryServerTest {

    @Test
    public void testStartStop() throws LDAPException {
        LdapServerMngt ldapServerMngt = new LdapServerMngt();
        try (InMemoryDirectoryServer server = ldapServerMngt.startServer()) {

            try (LDAPConnection connection = ldapServerMngt.getDirectConnection()) {
                LDAPTestUtils.assertEntryExists(connection, "");
                LDAPTestUtils.assertEntriesExist(connection, "uid=john,ou=people,dc=humanfork,dc=de");
            }

            //        // Get an unencrypted connection to the server's LDAP listener, then use
            //        // StartTLS to secure that connection.  Make sure the connection is usable
            //        // by retrieving the server root DSE.
            //        LDAPConnection connection = server.getConnection("LDAP");        
            //        connection.processExtendedOperation(new StartTLSExtendedRequest( clientSSLUtil.createSSLContext()));
            //        LDAPTestUtils.assertEntryExists(connection, "");
            //        connection.close();
            //
            //        // Establish an SSL-based connection to the LDAPS listener, and make sure
            //        // that connection is also usable.
            //        connection = server.getConnection("LDAPS");
            //        LDAPTestUtils.assertEntryExists(connection, "");
            //        connection.close();

            /* Shut down the server so that it will no longer accept client connections, and close all existing connections. */
            //server.shutDown(true); -- done by autoclosable

        }
    }

}
