package de.humanfork.experiment.ldapsdk;

import java.io.InputStream;

import com.unboundid.ldap.listener.InMemoryDirectoryServer;
import com.unboundid.ldap.listener.InMemoryDirectoryServerConfig;
import com.unboundid.ldap.listener.InMemoryListenerConfig;
import com.unboundid.ldap.sdk.LDAPConnection;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldif.LDIFReader;

/**
 * Hello world!
 */
public class LdapServerMngt {
    
    private InMemoryDirectoryServer server;
    
    private static final String LDAP_LISTENER = "LDAP";
    
    public InMemoryDirectoryServer startServer() throws LDAPException {
        /* Create a base configuration for the server. */
        InMemoryDirectoryServerConfig config = new InMemoryDirectoryServerConfig("dc=humanfork,dc=de");
        config.addAdditionalBindCredentials("cn=Admin", "password");

        config.setSchema(null); //disable schema validation
        
        /*
         * listen to port 10,389  (default is 389) at all network interfaces         
         * (use port 0 to let the configuration choose an available free port)
         * this method is overloaded, for example for ssl configuration - see code snippet at InMemoryDirectoryServer
         */
        InMemoryListenerConfig listenerConfig = InMemoryListenerConfig.createLDAPConfig(LDAP_LISTENER, 10_389); //LDAP default port is 389
        config.setListenerConfigs(listenerConfig);
        
        /* Create and start the server instance and populate it with an initial set of data from an LDIF file. */
        this.server = new InMemoryDirectoryServer(config);
        this.server.importFromLDIF(true, new LDIFReader(ldifContent()));

        // Start the server so it will accept client connections.
        this.server.startListening();
        return server;
    }
    
    
    private static InputStream ldifContent() {
        InputStream ldifContent = InMemoryDirectoryServer.class.getClassLoader().getResourceAsStream("schema.ldif");
        if (ldifContent==null) {
            throw new RuntimeException("schema.ldif not found");
        }
        return ldifContent;
    }
    
    public void shutDown() {
        server.shutDown(true);
    }
    
    /** Provides an ldap connection that is directly linked to the ldap listener. */
    public LDAPConnection getDirectConnection() throws LDAPException {
        return server.getConnection(LDAP_LISTENER);
    }
}
